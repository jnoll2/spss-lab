## jhnoll@gmail.com
BIB=references.bib
PANDOC=pandoc --filter=csv_to_table --bibliography=$(BIB)
# The Generic preprocessor (with pandoc in mind) https://github.com/CDSoft/pp
PP=pp

all: report

%.docx: %.md
	$(PP) $< | $(PANDOC) --standalone -t docx -o $@

%.rtf: %.md
	$(PP) $< | $(PANDOC) --standalone -t rtf -o $@

%.odt: %.md
	$(PP) $< | $(PANDOC) --standalone -t odt -o $@

%.tex: %.md
	$(PP) $< | $(PANDOC) --standalone -t latex -o $@

%.pdf: %.md Makefile
	$(PP) $< | $(PANDOC) --standalone  -t latex -o $@

%.html: %.md Makefile
	$(PP) $< | $(PANDOC) --standalone  -t html -o $@ 


# XXX This will only work when the repo is cloned in a topic directory.
%.md: %.in Makefile
	$(PP) ../../../macros.pp ../../../config.pp $< | $(PANDOC) --standalone  -t markdown -o $@ 
