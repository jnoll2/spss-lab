---
bibliography: 'references.bib'
module: 7COM1079
subtitle: '7COM1079 -- Team Research and Development Project'
title: 'Descriptive stats -- spss lab'
---

Instructions for completing this practical exercise:

1.  Compile the lab instructions

    1.  If you are reading this *README.md* file from the web, open a
        Command shell and change to the Git workspace containing this
        file, that you just cloned:

        1.  Click the "windows" button, type "command" then open a "DOS
            box" (the black window).

        2.  At the command prompt, type:

                 cd spss-lab

            *Note:* If you did not clone the spss-lab repository in your
            "home" directory, you should type:

                 cd \Users\yourusername\path\to\spss-lab

            **IMPORTANT!** replace "\\path\\to" above with the path to
            the directory into which you cloned the spss-lab repository.

    2.  In the command window, type:

             pandoc --standalone -t html -o instructions.html instructions.md

    3.  Now, just type:

             instructions.html

        to view the instructions in a web browser.

2.  Read and follow the instructions.
